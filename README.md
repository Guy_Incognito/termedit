TermEdit is a simple Terminal Application for editing Text/Code files.

Libraries needed (for building application)

* base (>=4)

* data-default (>= 0.5.3)

* vty (>= 5.2.1)

* split (>= 0.2.2)

* strict (>= 0.3.2)

* directory (>= 1.2.0)