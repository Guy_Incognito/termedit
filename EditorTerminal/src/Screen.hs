module Screen where

import Common

import Data.List.Split
import Graphics.Vty


delimiterList :: [Textelement]
delimiterList = [('\n',False,0),('\n',True,0)
                ,('\n',False,1),('\n',True,1)
                ,('\n',False,2),('\n',True,2)
                ,('\n',False,3),('\n',True,3)
                ,('\n',False,4),('\n',True,4)
                ,('\n',False,5),('\n',True,5)]


--prints the text
refreshText :: Text -> Vty -> IO()
refreshText txt vty = do
        let
                -- split by newlines, but don't care if cursor is True or False
                img2 = createImage (split (keepDelimsR $ oneOf delimiterList) txt)
                pic = Picture (NoCursor) [img2] ClearBackground
        update vty pic
        
--prints given lines of the text
refreshTextArea :: Text -> RowInterval -> Vty -> IO()
refreshTextArea txt (from,to) vty = do
        let
                -- split by newlines, but don't care if cursor is True or False
                textLines = split (keepDelimsR $ oneOf delimiterList) txt       
                temp1 = drop from textLines
                temp2 = take (to-from) temp1
                img2 = createImage temp2
                pic = Picture (NoCursor) [img2] ClearBackground
        update vty pic        

--prints the text and displays a notification in the foreground
refreshTextWithNotification :: Text -> Image -> Vty -> IO()
refreshTextWithNotification txt notification vty = do
        let
                -- split by newlines, but don't care if cursor is True or False
                img2 = createImage (split (keepDelimsR $ oneOf delimiterList) txt)
                pic = Picture (NoCursor) [img2] ClearBackground
                pic2 = addToTop pic notification
        update vty pic2

-- creates the visual representation
createImage :: [Text] -> Image
createImage [] = emptyImage
createImage (x:xs) = vertCat [createImageSub x , createImage (xs)]  
    
createImageSub :: Text -> Image
createImageSub [] = emptyImage
createImageSub (x:xs)  
        | (getSecond x == True) = char (withBackColor (withForeColor defAttr black) white) (getFirst x) <|> createImageSub (xs) 
        | (getThird x == 1) = char (withForeColor defAttr red) (getFirst x) <|> createImageSub (xs)  
        | (getThird x == 2) = char (withForeColor defAttr blue) (getFirst x) <|> createImageSub (xs)  
        | (getThird x == 3) = char (withForeColor defAttr yellow) (getFirst x) <|> createImageSub (xs) 
        | (getThird x == 4) = char (withForeColor defAttr green) (getFirst x) <|> createImageSub (xs)  
        | (getThird x == 5) = char (withForeColor defAttr magenta) (getFirst x) <|> createImageSub (xs)                        
        | otherwise = char defAttr (getFirst x) <|> createImageSub (xs)  
        
        
        
rainbow :: Text -> Text                   
rainbow txt = rainbowSub txt 0

rainbowSub :: Text -> Int -> Text  
rainbowSub [] _ = []
rainbowSub (x:xs) counter 
        | counter == 0 = setTextelementColor x 0 : rainbowSub xs 1
        | counter == 1 = setTextelementColor x 1 : rainbowSub xs 2
        | counter == 2 = setTextelementColor x 2 : rainbowSub xs 3
        | counter == 3 = setTextelementColor x 3 : rainbowSub xs 4
        | counter == 4 = setTextelementColor x 4 : rainbowSub xs 5
        | counter == 5 = setTextelementColor x 5 : rainbowSub xs 0 
        | otherwise = x : rainbowSub xs 0  
        
        
        
-- Set the Color of Textelement
setTextelementColor :: Textelement -> Int -> Textelement
setTextelementColor (x,y,z) color = (x,y,color)   


helpText :: Image
helpText = vertCat $ map (string defAttr)
    [ ""
    , "         #######                         #######                "
    , "            #    ###### #####  #    #    #       #####  # ##### "    
    , "            #    #      #    # ##  ##    #       #    # #   #   "
    , "            #    #####  #    # # ## #    #####   #    # #   #   "
    , "            #    #      #####  #    #    #       #    # #   #   "
    , "            #    #      #   #  #    #    #       #    # #   #   "
    , "            #    ###### #    # #    #    ####### #####  #   #   "
    , "------------------------------------------------------------------------"
    , "COMMANDS: "
    , "STRG-S -> save file"
    , "STRG-H -> display help screen"  
    , "STRG-R -> rainbow mode!"  
    , "ESC -> EXIT"    
    , "PAGE UP -> scroll up"   
    , "PAGE DOWN -> scroll down" 
    , "------------------------------------------------------------------------"

    ]
    


--TODO make this centered    
savedText :: Image
savedText  = vertCat $ map (string defAttr)
    [ "------------------------------------------------------------------------"
    , "------------------------------ File saved ------------------------------"
    , "------------------------------------------------------------------------"
    ]  
    
    
    