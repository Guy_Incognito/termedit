module Main where

import Common
import Cursor
import TextEdit
import Screen
import SyntaxChecking


import qualified System.IO.Strict as S
import System.Exit
import System.Environment
import System.Directory

import Graphics.Vty

import Data.Default (def)



--constants

tempFileName :: String
tempFileName ="../temp.txt" 

tempViewFileName :: String
tempViewFileName ="../tempView.txt" 

scrollStep :: Int
scrollStep = 10

--main function
main::IO()
main = do

     --get Arguments
     args <- getArgs
     
     --get FileName from args
     let filePath = head args

     --read SourceFile
     src <- readFile filePath
     
     --create internal representation
     let txt = setCursorAt (createTextRepresentation src) 0

     --create temporary file
     let txt2 = getHighlight txt
     writeTempFile txt2
     
     -- create default view range
     let defaultView = (0,40)
     writeFile tempViewFileName (show defaultView) 
        
     --setup Display
     vty <- mkVty def
     
     -- display welcome screen
     let pic = Picture (NoCursor) [helpText] ClearBackground
     update vty pic          
     _ <- nextEvent vty
     
     commandLoop vty filePath


commandLoop :: Vty -> String -> IO()
commandLoop vty filePath = do
              
              srcTxt <- readTempFile
           
              -- refresh display
              temp <- S.readFile tempViewFileName
              let view = read temp :: RowInterval
              refreshTextArea srcTxt view vty 
                
              -- event handling  
              e <- nextEvent vty
              handleEvent e srcTxt vty filePath
              commandLoop vty filePath      

         
readTempFile :: IO Text
readTempFile = do
                    temp <- S.readFile tempFileName
                    return (read temp)

writeTempFile ::  Text -> IO()
writeTempFile txt =  writeFile tempFileName (show txt)

writeTempFileAndCheckSyntax  ::  Text -> IO()
writeTempFileAndCheckSyntax txt = do
                                        let txt2 = getHighlight txt
                                        writeFile tempFileName (show txt2) 
                    
handleEvent :: Event -> Text -> Vty -> String -> IO()
handleEvent e txt vty filePath
        
        | e == (EvKey KPageDown []) = scrollDown txt vty
        | e == (EvKey KPageUp []) = scrollUp txt vty
        
        | e == (EvKey KRight []) = writeTempFile (setCursorToNextPosition txt)         
        | e == (EvKey KLeft []) = writeTempFile (setCursorToPreviousPosition txt)      
        | e == (EvKey KUp []) = writeTempFile (moveCursorUp txt) 
        | e == (EvKey KDown []) = writeTempFile (moveCursorDown txt) 
        
        | e == (EvKey KBS []) = writeTempFileAndCheckSyntax (removeChar txt ((getCursorPosition txt)-1))       
        | e == (EvKey KDel []) = writeTempFileAndCheckSyntax (removeChar txt ((getCursorPosition txt)+1)) 
          
        | e == EvKey KEnter [] = writeTempFileAndCheckSyntax (insertChar txt '\n' (getCursorPosition txt))
        | e == EvKey (KChar '\t') [] = writeTempFileAndCheckSyntax (insertString txt "   " (getCursorPosition txt))  
                                                           
        | e == (EvKey KEsc []) =  exitProgram vty
                                          
        | e == EvKey (KChar 'r') [MCtrl] = writeTempFile (rainbow txt)
                                        
        -- save the file  with CTRL-S                     
        | e == EvKey (KChar 's') [MCtrl] = do
                                           writeFile filePath (createStringRepresentation txt)     
                                           refreshTextWithNotification txt savedText vty
                                           _ <- nextEvent vty
                                           commandLoop vty filePath 
        -- display Help with CTRL-H 
        | e == EvKey (KChar 'x') [MCtrl] = do
                                           let pic = Picture (NoCursor) [helpText] ClearBackground
                                           update vty pic          
                                           _ <- nextEvent vty
                                           commandLoop vty filePath                 
        
        | otherwise = do 
                        let input = getCharFromKeyEvent e
                        case input of
                                Just value       -> writeTempFileAndCheckSyntax (insertChar txt value (getCursorPosition txt))
                                Nothing          -> return()


scrollDown :: Text -> Vty -> IO ()
scrollDown txt vty = do
                        temp <- S.readFile tempViewFileName
                        let (from,to) = read temp :: RowInterval
                        let newView = (from+scrollStep,to+scrollStep)
                        refreshTextArea txt newView vty
                        writeFile tempViewFileName (show newView) 

scrollUp :: Text -> Vty -> IO ()
scrollUp txt vty = do
                        temp <- S.readFile tempViewFileName
                        let (from,to) = read temp :: RowInterval
                        let newView = scrollUpNewView (from,to)
                        refreshTextArea txt newView vty
                        writeFile tempViewFileName (show newView) 

scrollUpNewView :: RowInterval -> RowInterval
scrollUpNewView (from,to)
        | (from - scrollStep) <0 = (0,to)
        |otherwise  = (from-scrollStep,to-scrollStep)

-- parses the char from the received event
getCharFromKeyEvent :: Event -> Maybe Char
getCharFromKeyEvent event 
        | (take 8 txt) == "EvResize"    = Nothing
        |  ((length txt) > 14)          = Just $ txt !! 14
        | otherwise                     = Nothing
        where txt = show event


exitProgram :: Vty -> IO ()
exitProgram vty = do
                        shutdown vty
                        removeFile tempFileName
                        removeFile tempViewFileName
                        exitSuccess
