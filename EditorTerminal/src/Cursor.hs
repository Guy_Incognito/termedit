module Cursor where

import Common

-- marks a Textelement as current cursor position
setCursor :: Textelement -> Textelement
setCursor (a,_,c) = (a,True,c)

setNotCursor :: Textelement -> Textelement
setNotCursor (a,_,c) = (a,False,c)

isCursor :: Textelement -> Bool
isCursor (_,True,_) = True
isCursor (_,False,_) = False

--sets cursor at given position
setCursorAt :: Text -> Int -> Text
setCursorAt text pos = setCursorAtSub text pos 0

setCursorAtSub :: Text -> Int -> Int -> Text
setCursorAtSub text pos counter 
        | null text = []
        | counter == pos = setCursor (head text) : tail text
        | otherwise     = head text : setCursorAtSub (tail text) pos (counter+1)   
        
        
        
--sets cursor to next position        
setCursorToNextPosition :: Text -> Text
setCursorToNextPosition [] = []
setCursorToNextPosition (x:y:xxs)
        | isCursor x  = setNotCursor x : setCursor y : xxs
        | otherwise = x: setCursorToNextPosition (y:xxs)     
setCursorToNextPosition [x] = [x]           




--sets cursor to previous position        
setCursorToPreviousPosition :: Text -> Text
setCursorToPreviousPosition [] = []
setCursorToPreviousPosition (x:y:xxs)
        | isCursor y  = setCursor x : setNotCursor y : xxs
        | otherwise = x: setCursorToPreviousPosition (y:xxs)
setCursorToPreviousPosition [x] = [x]           

-- moves cursor to upper row
moveCursorUp :: Text -> Text
moveCursorUp txt = do
                   let (line,pos) = getPositionAndLine txt
                   setCursorTo txt (line-1) pos

-- moves cursor to lower row
moveCursorDown :: Text -> Text
moveCursorDown txt = do
                   let (line,pos) = getPositionAndLine txt
                   setCursorTo txt (line+1) pos
                   
--returns current cursor position
getCursorPosition :: Text -> Int
getCursorPosition txt =  getCursorPositionSub txt 0

getCursorPositionSub :: Text -> Int -> Int
getCursorPositionSub [] _ = 0
getCursorPositionSub (x:xs) counter
        | (isCursor x) = counter
        | otherwise = getCursorPositionSub xs (counter+1)
        
        
        
-- returns the cursors line and position (in that line)       
getPositionAndLine :: Text -> (Int,Int)
getPositionAndLine txt = getPositionAndLineSub txt 0 0

getPositionAndLineSub :: Text -> Int -> Int -> (Int,Int)
getPositionAndLineSub [] _ _ = (0,0)
getPositionAndLineSub (x:xs) line position 
        | (isCursor x) = (line,position)
        | (getFirst x == '\n') =  getPositionAndLineSub xs (line+1) 0
        | otherwise =  getPositionAndLineSub xs line (position+1)      



--returns the first element from the given line
getFirstElementOfLine :: Text -> Int -> Maybe Textelement
getFirstElementOfLine txt line =  getFirstElementOfLineSub txt line 0 

getFirstElementOfLineSub :: Text -> Int -> Int -> Maybe Textelement
getFirstElementOfLineSub txt line lineCounter 
        | null txt = Nothing
        | (lineCounter == line) = Just (head txt)
        | getFirst (head txt) =='\n' = getFirstElementOfLineSub (tail txt) line (lineCounter +1)
        | otherwise = getFirstElementOfLineSub (tail txt) line  (lineCounter)



-- sets cursor at (line,position)        
setCursorTo :: Text -> Int -> Int -> Text
setCursorTo txt line pos 
        | (line <0) = txt
        | (line > ((getNumberOfLines txt)-2) ) = txt
        | otherwise = setCursorToSub txt line pos 0 0 

setCursorToSub :: Text -> Int -> Int -> Int -> Int -> Text
setCursorToSub txt line pos elemCounter lineCounter 
        | null txt = []
        | (lineCounter == line) &&  (elemCounter == pos) = (setCursor (head txt)) : setCursorToSub (tail txt) line pos (elemCounter+1) (lineCounter+1)
        -- if end of line is reached
        | (lineCounter == line) &&  getFirst (head (tail txt)) == '\n' = (setCursor (head txt)) : setCursorToSub (tail txt) line pos (elemCounter+1) (lineCounter+1)
        | (lineCounter == line) =  (setNotCursor (head txt)) : setCursorToSub (tail txt) line pos (elemCounter+1) (lineCounter)
        | getFirst (head txt) =='\n' = (setNotCursor (head txt)) :  setCursorToSub (tail txt) line pos (elemCounter) (lineCounter +1)
        | otherwise = (setNotCursor (head txt)) : setCursorToSub (tail txt) line pos (elemCounter) (lineCounter)
