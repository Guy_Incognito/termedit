module SyntaxChecking where

import Common
import Data.Array
  ( Array
  , listArray
  , bounds
  , (!)
  )

import Data.List
import Data.Char

-- |The solid data type of KMP table
data Table a = Table
  { alphabetTable :: Array Int a
  , jumpTable :: Array Int Int
  }

--  Color
-- 1 ... red     ... error
-- 2 ... blue    ... keywords
-- 3 ... yellow  ... variables
-- 4 ... green   ... Strings

reservedKeyWords   = [" split ", " exec ", " finally "]


-- MAIN Func -------------
getHighlight :: Text -> Text
getHighlight text = highlightedText
		-- Refresh and set Text to black
	where 	refreshedText   = setColor text 0
		-- Convert Text to String
		content         = createStringRepresentation refreshedText
		-- Set Highlighting
		highlightedText = setHighlight refreshedText content

setHighlight :: Text -> [Char] -> Text
setHighlight text content = markedtext6
	where 	listLines   = splitOn' (=='\n') content
		lengthLines = map length listLines
		struct      = syntax listLines lengthLines 0 1
		linesError  = checkLine struct
		-- highlight Strings
		markedtext1   = highlightString text struct
		markedtext2   = highlightUnfinished markedtext1 struct
		-- highlight KeyWords and Variables
		variables     = findVariable $ filter (\line -> (length line > 1)) listLines
		markedtext3   = highlightKeyWords markedtext2 content variables        3
		markedtext4   = highlightKeyWords markedtext3 content reservedKeyWords 2
		-- highlight incorrect lines
		markedtext5    = highlightErr markedtext4 struct linesError
		-- highlight Brackets if necessary
		markedtext6   = highlightBrackets markedtext5



-- s ... Char; b ... Bool; c ... color
setColor :: [Textelement] -> Int -> [Textelement]
setColor [] color = []
setColor ((s,b,c):xs) color = ( (s,b,color):(setColor xs color) )

setColorForOne :: Textelement -> Int -> Textelement
setColorForOne (s,b,c) color= (s,b,color)

-- Highlight Strings
highlightString :: [Textelement] -> [(t, Int, t1, [a])] -> [Textelement]
highlightString text [] = text
highlightString text ((num,start,end,line):struct) = highlightString (a ++ (lookForString b False) ++ c) struct
	where 	(a,z) = splitAt start 	      text
		(b,c) = splitAt (length line) z

lookForString :: Text -> Bool -> Text
lookForString [] bool = []
lookForString (y:[]) bool = (y:[])
lookForString (y:t:text) bool
	| c == '\"' && bool == False && pre /= '\\' = (y : (lookForString ((setColorForOne t 4):text) True))
	| c /= '\"' && bool                         = (y : (lookForString ((setColorForOne t 4):text) True))
	| c == '\"' && bool          && pre == '\\' = (y : (lookForString ((setColorForOne t 4):text) True))
	| c == '\"' && bool                         = (y : (lookForString ((setColorForOne t 4):text) False))
	| otherwise                                 = (y : (lookForString (t:text) False))
	where 	c   = getFirst t
		pre = getFirst y

highlightUnfinished text [] = text
highlightUnfinished text ((n,start,end,l):struct)
	| (getThird elem) == 4 = highlightUnfinished (highlightQuote text start (length l)) struct
	| otherwise            = highlightUnfinished text struct
	where 	elem = text !! (end-1)

highlightQuote text start end = (a ++ (markQuote b ) ++ c)
	where 	(a,z) = splitAt start text
		(b,c) = splitAt end   z	
markQuote [] = []
markQuote (elem:text)
	| c == '\"' = (setColorForOne elem 1) : markQuote text
	| otherwise = elem                    : markQuote text
	where c = getFirst elem	

-- Highlight Complete Line if it does not end correctly
highlightErr :: Text -> [(t, Int, t1, [a])] -> [Int] -> Text
highlightErr text struct [] = text
highlightErr text struct (x:xs) = highlightErr (a ++ (setColor b 1) ++ c) struct xs
	where 	(num,start,end,line) = struct !! (x-1) --because of Array -1
		(a,z) = splitAt start 	    	text
		(b,c) = splitAt (length line)   z


-- Highlight KeyWords and Variables
highlightKeyWords :: [Textelement] -> [Char] -> [[Char]] -> Int-> [Textelement]
highlightKeyWords text content keywords color = highlight2 text matchresult keywords color
	where 	kmpTables   = map build keywords
		matchresult = map (findword content) kmpTables

findword content word = match word content

findVariable :: [String] -> [String]
findVariable []     = []
findVariable (line:rest)
	| pos > 0   = (findVariable' (take pos elements)) ++ findVariable rest
	| pos2> 0   = (findVarInFuncHeader (drop 1 (take pos2 elements)))++ findVariable rest
	| otherwise = findVariable rest
	where 	elements = words line
		pos = getPos $ reverse $ map (/="=") elements
		pos2= getPos $ reverse $ map (/="{") elements


findVarInFuncHeader elements = selectedfinished
	where 	pos = getPos $ reverse $ map (=="-") elements
		selected = take (pos-1) elements 
		ready1 = map (" "++) $ map (++" ") selected
		ready2 = map ("$"++) $ map (++"$") selected
		selectedfinished = ready1 ++ ready2

findVariable' [] = []
findVariable' (x:xs)
	| (length check3) > 0 = findVariable' xs
	| otherwise          = (" "++x++" ") : ("$"++x++"$") : (findVariable' xs)
	where 	check1 = filter (\c -> not (elem c ['a'..'z'])) x
		check2 = filter (\c -> not (elem c ['A'..'Z'])) check1
		check3 = filter (\c -> not (elem c ['1'..'9'])) check2

-- Highlight Items and Variables
highlight2 text [] [] _ = text
highlight2 text (m:matchl) (i:iteml) color = highlight2 (highlight2' text m i color) matchl iteml color

highlight2' text [] _ _ = text
highlight2' text (pos:rest) item color = highlight2' (a ++ (setColor b color) ++ c) rest item color
	where 	(a,z) = splitAt pos 	    	text
		(b,c) = splitAt (length item)   z


-- End of Line must be either '{' or '}' or "" or ";"
checkLine lines = filter (>0) $ map checkLine' (filter (\(x,y,z,line) -> (length line > 1)) lines)

checkLine' (number,start,end,line)
	| onlySpaces == 0 = -1
	| char == '{' || char == '}' || char == ';' = -1
	| otherwise   = number
	where 	pos        = getPos $ reverse $ map isSpace line
		char       = line !! pos
		onlySpaces = length $ filter (==False) $ (map isSpace line)

-- List is given in Reverse to find 
getPos [] = -1
getPos (x:xs)
	| x 	    = getPos xs
	| otherwise = length xs
	
-- Highlight Bracket with RED if it is missing
highlightBrackets text = highlightBracket text (-1) 0
highlightBracket text openb counter
	| counter == (length text) && openb == -1 = text
	| counter == (length text) && openb /= -1 = highlightBracket' openb text
	| c == '{' && openb == -1 = highlightBracket text counter (counter+1)
	| c == '{' && openb /= -1 = highlightBracket (highlightBracket' openb text) counter (counter+1)
	| c == '}' && openb /= -1 = highlightBracket text (-1) (counter+1)
	| c == '}' && openb == -1 = highlightBracket (highlightBracket' counter text) (-1) (counter+1)
	| otherwise               = highlightBracket text openb (counter+1)
	where 	x = text !! counter
		c = getFirst x

highlightBracket' pos text = left ++ [setColorForOne x 1] ++ right
	where 	x = text !! pos
		left = take (pos) text
		right= drop (pos+1) text

-- ---------------------------------------------
-- (start, end, line)
-- Bsp (number,302,303,"}"),(number,304,304,""),(number,305,342,"handle_illegal_chars path - \"$out$\" {"),
-- auf Position 302 ist '}'
-- auf Position 303 ist '\n'
-- auf Position 304 ist '\n'
-- auf Position 305 ist 'h'
-- auf Position 341 ist '{'
syntax [] [] _ _ = []
syntax lines length start number = (number, start,endOfLine,head lines) : (syntax (tail lines) (tail length) newstart (number+1))
	where 	newstart = start + (head length) + 1
		endOfLine = newstart - 1

-- this variant discards separators but does not combine adjacent separators
-- splitOn' (==',') "foo,bar,,,baz" = ["foo", "bar", "", "", "baz"]
-- this is the behavior you want for List.lines
--
splitOn' :: (a -> Bool) -> [a] -> [[a]]
splitOn' f xs = split xs
  where split xs = case break f xs of
          (chunk,[])     -> chunk : []
          (chunk,_:rest) -> chunk : split rest


-- ----------- KMP -----------------
-- The 'build' function eats a pattern (list of some Eq) and generates a KMP table.
-- The time and space complexities are both O(length of the pattern)
build :: Eq a => [a] -> Table a
build pattern =
  let
    len = length pattern
    resTable = Table
      { alphabetTable = listArray (0,len-1) pattern
      , jumpTable = listArray (-1,len-1) $ (-2) : genJump (-1) 0
      }

    genJump _ 0 =
      let
        o = if 1 == len || alphabetTable resTable ! 0 /= alphabetTable resTable ! 1
          then -1
          else -2

        later = genJump (-1) 1
      in
        o : later

    genJump lashighlight2'Jump i =
      let
        ch = alphabetTable resTable ! i

        findJ j
          | j == -2 = -2
          | alphabetTable resTable ! (j+1) == ch = j
          | j == -1 = -2
          | otherwise = findJ (jumpTable resTable ! j)

        j = findJ lashighlight2'Jump

        o = if i+1 == len || alphabetTable resTable ! (i+1) /= alphabetTable resTable ! (j+2)
          then j+1
          else jumpTable resTable ! (j+1)

        later = genJump (j+1) (i+1)
      in o : later

  in
    resTable

-- The 'match' function takes the KMP table and a list to be searched (might be infinite)
-- and then generates the search results as a list of every matched begining (might be infinite).
match :: Eq a => Table a -> [a] -> [Int]
match table str =
  let
    len = 1 + snd ( bounds (alphabetTable table) )

    go i j str =
      let
        later = case str of
          (s:ss) ->
            let (i', j', str')
                	| j < 0 || j < len && s == alphabetTable table ! j = (i + 1, j + 1, ss)
                	| otherwise = (i, 1 + (jumpTable table ! (j - 1)), str)
            in
              go i' j' str'
          _ -> []
      in
        if j == len then i-len : later else later
  in
    go 0 0 str

