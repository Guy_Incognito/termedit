module TextEdit where

import Common

--inserts char AT given position
insertChar :: Text -> Char -> Int -> Text
insertChar text char pos = insertCharSub text char pos 0

insertCharSub :: Text -> Char -> Int -> Int -> Text
insertCharSub text char pos counter 
        | null text = []
        | counter == pos = (char,False,0) : text
        | otherwise     = head text : insertCharSub (tail text) char pos (counter+1)
   
   
--inserts String AT given position
insertString :: Text -> String -> Int -> Text
insertString text string pos = insertStringSub text string pos 0

insertStringSub :: Text -> String -> Int -> Int -> Text
insertStringSub text string pos counter 
        | null text = []
        | counter == pos = (createTextRepresentation string) ++ text
        | otherwise     = head text : insertStringSub (tail text) string pos (counter+1)   
    
 --removes char AT given position       
removeChar :: Text -> Int -> Text
removeChar text pos 
        | (pos <0) = text
        | otherwise = removeCharSub text pos 0

removeCharSub :: Text -> Int -> Int -> Text
removeCharSub text pos counter 
        | null text = []
        | counter == pos = tail text
        | otherwise     = head text : removeCharSub (tail text) pos (counter+1)   

