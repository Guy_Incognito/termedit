module Common where

-- Textelement represents one Char in the text
-- (Char,isCursor,Color)
type Textelement = (Char,Bool,Int)
type Text = [Textelement]

-- rows (from, to)
type RowInterval = (Int,Int)

--access functions
getFirst :: (a, b, c) -> a
getFirst (a,_,_) = a

getSecond :: (a, b, c) -> b
getSecond (_,b,_) = b

getThird :: (a, b, c) -> c
getThird (_,_,c) = c

createStringRepresentation :: Text -> String
createStringRepresentation txt = map getFirst txt

--creates a list of unformated text
createTextRepresentation :: String -> Text
createTextRepresentation [] = []
createTextRepresentation text = (head text,False,0) : createTextRepresentation (tail text)

-- returns the number of lines
getNumberOfLines :: Text -> Int
getNumberOfLines txt = getNumberOfLinesSub txt 0
  
getNumberOfLinesSub  :: Text -> Int -> Int
getNumberOfLinesSub [] lineCounter = lineCounter
getNumberOfLinesSub (x:xs) lineCounter 
        | (getFirst x) == '\n' = getNumberOfLinesSub xs (lineCounter+1)
        | otherwise = getNumberOfLinesSub xs lineCounter
        
