logpath a - "$path$" {
    path = "tmp.txt";
}

    
replace_all search replace subject - "$out$" {
    head tail = split "$search$" "$subject$";
    tail == "" : out = "$head$";
    head != "" : newTail = replace_all "$search$" "$replace$" "$tail$";
    head != "" : out = "$head$$replace$$newTail$";
}

handle_illegal_chars path - "$out$" {
    a = replace_all ":" " " "$path$";
    b = replace_all "\\"" " " "$a$";
    c = replace_all "*" " " "$b$";
    d = replace_all "<" " " "$c$";
    e = replace_all ">" " " "$d$";
    out = "$e$";
}

log message - "$out$" {
    path = logpath " ";
    res = exec "echo $message$ >> $path$";
    res == "0" : out = "0";
    res != "0" : out = log "$message$";
}

clear_log message - "$res$" {
    path = logpath " ";
    res = exec "echo start log > $path$";
}

get_files directory - "$list$" {
    success raw = exec "dir $directory$ /A:-D /B";
    begin d = split "\\n" "$raw$";
    l = log "$success$ - $raw$ - $begin$";
    l : list = "$raw$";
}
    
get_attribute_from_id3 id3 attribute - "$out$" {
    tmp1 tmp2 = split "$attribute$ " "$id3$";
    tmp3 tmp4 = split " " "$tmp2$";
    out tmp5 = split "\\r\\n" "$tmp4$";
}    
    
get_path_by_id3 path - "$out$" {
    taggerSharp = "..\\\\demofolder\\\\TaggerSharp";
    success output = exec "$taggerSharp$.exe \\"$path$\\"";
    title = get_attribute_from_id3 "$output$" "Title";
    artist = get_attribute_from_id3 "$output$" "Performers";
    album = get_attribute_from_id3 "$output$" "Album";
    newPath = "$artist$\\$album$\\$title$.mp3";
    newPath1 = handle_illegal_chars "$newPath$";
    t = log "moved file: $path$ -- COPY TO -- $newPath1$";
    t : out = "$newPath1$";
}    
    
handle_single_mp3 path - "$out2$" {
    mp3Path = get_path_by_id3 "$path$";
    out2 res = exec "echo D|xcopy /Y \\"$path$\\" \\"..\\\\new\\\\$mp3Path$\\"";
}

handle_mp3_files files path - "$out$" {
    files == "" : done = "True";
    files != "" : done = "False";
    done == "True" : head = "Done";
    done == "False" : head tail = split "\\n" "$files$";
    head != "Done" : t1 = handle_single_mp3 "$path$\\$head$";
    head != "Done" : out = handle_mp3_files "$tail$" "$path$";
    head == "Done" : out = "Done";
}
    
handle_all_mp3s dir - "$out$" {
    t = clear_log " ";
    t : f = get_files "$dir$\\\\*.mp3";
    out = handle_mp3_files "$f$" "$dir$";
}
    
res = handle_all_mp3s "..\\\\demofolder";     

